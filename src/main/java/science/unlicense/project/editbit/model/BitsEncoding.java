
package science.unlicense.project.editbit.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Primitive;

/**
 *
 * @author Johann Sorel
 */
public class BitsEncoding extends PrimitiveEncoding{

    public BitsEncoding() {
        super(Primitive.UINT8, Endianness.BIG_ENDIAN);
    }

    @Override
    public Chars getName() {
        return new Chars("Bits");
    }

}
