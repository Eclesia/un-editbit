
package science.unlicense.project.editbit.model;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Bits;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Primitive;
import static science.unlicense.common.api.number.Primitive.*;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class PrimitiveEncoding implements DataEncoding {

    private final int type;
    private final Endianness enc;

    public PrimitiveEncoding(int type, Endianness enc) {
        this.type = type;
        this.enc = enc;
    }

    public Endianness getNumberEncoding() {
        return enc;
    }

    public Chars getName() {
        Chars candidate = new Chars("");
        switch(type){
            case BITS1     : candidate = new Chars("U-1Bit"); break;
            case BITS2     : candidate = new Chars("U-2Bit"); break;
            case BITS4     : candidate = new Chars("U-4Bit"); break;
            case INT8      : candidate = new Chars("S Int  8"); break;
            case UINT8     : candidate = new Chars("U Int  8"); break;
            case INT16     : candidate = new Chars("S Int 16"); break;
            case UINT16    : candidate = new Chars("U Int 16"); break;
            case INT24     : candidate = new Chars("S Int 24"); break;
            case UINT24    : candidate = new Chars("U Int 24"); break;
            case INT32       : candidate = new Chars("S Int 32"); break;
            case UINT32      : candidate = new Chars("U Int 32"); break;
            case INT64      : candidate = new Chars("S Int 64"); break;
            case UINT64     : candidate = new Chars("U Int 64"); break;
            case FLOAT32     : candidate = new Chars("Float 32"); break;
            case FLOAT64    : candidate = new Chars("Float 64"); break;
        }
        if(type>5){
            if(enc==Endianness.BIG_ENDIAN){
                candidate = candidate.concat(new Chars(" BE"));
            }else{
                candidate = candidate.concat(new Chars(" LE"));
            }
        }
        return candidate;
    }

    public int getBitSize() {
        return Primitive.getSizeInBits(type);
    }

    public Object decode(byte[] buffer, int offset) throws IOException {
        Object candidate = new Chars("");
        switch(type){
//            case BITS1     : candidate = ds.readBits(1); break;
//            case BITS2     : candidate = ds.readBits(2); break;
//            case BITS4     : candidate = ds.readBits(4); break;
            case INT8      : candidate = enc.readByte(buffer, offset); break;
            case UINT8     : candidate = enc.readUByte(buffer, offset); break;
            case INT16     : candidate = enc.readShort(buffer, offset); break;
            case UINT16    : candidate = enc.readUShort(buffer, offset); break;
            case INT24     : candidate = enc.readInt24(buffer, offset); break;
            case UINT24    : candidate = enc.readUInt24(buffer, offset); break;
            case INT32       : candidate = enc.readInt(buffer, offset); break;
            case UINT32      : candidate = enc.readUInt(buffer, offset); break;
            case INT64      : candidate = enc.readLong(buffer, offset); break;
            case UINT64     : candidate = enc.readLong(buffer, offset); break;
            case FLOAT32     : candidate = enc.readFloat(buffer, offset); break;
            case FLOAT64    : candidate = enc.readDouble(buffer, offset); break;
        }
        return candidate;
    }

    public Object decode(DataInputStream ds) throws IOException {
        byte[] buffer = new byte[getBitSize()/8];
        ds.readFully(buffer);
        return decode(buffer, 0);
    }

    @Override
    public Number read(DataInputStream ds) throws IOException {
        Number candidate = 0;
        switch(type){
//            case BITS1     : candidate = ds.readBits(1); break;
//            case BITS2     : candidate = ds.readBits(2); break;
//            case BITS4     : candidate = ds.readBits(4); break;
            case INT8      : candidate = ds.readByte(); break;
            case UINT8     : candidate = ds.readUByte(); break;
            case INT16     : candidate = ds.readShort(enc); break;
            case UINT16    : candidate = ds.readUShort(enc); break;
            case INT24     : candidate = ds.readInt24(enc); break;
            case UINT24    : candidate = ds.readUInt24(enc); break;
            case INT32       : candidate = ds.readInt(enc); break;
            case UINT32      : candidate = ds.readUInt(enc); break;
            case INT64      : candidate = ds.readLong(enc); break;
            case UINT64     : candidate = ds.readLong(enc); break;
            case FLOAT32     : candidate = ds.readFloat(enc); break;
            case FLOAT64    : candidate = ds.readDouble(enc); break;
        }
        return candidate;
    }

    @Override
    public Class getValueClass() {
        switch(type){
            case BITS1     :
            case BITS2     :
            case BITS4     :
            case INT8      : return Byte.class;
            case UINT8     :
            case INT16     : return Short.class;
            case UINT16    :
            case INT24     :
            case UINT24    :
            case INT32       : return Integer.class;
            case UINT32      :
            case INT64      :
            case UINT64     : return Long.class;
            case FLOAT32     : return Float.class;
            case FLOAT64    : return Double.class;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public Number getMinValue() {
        switch(type){
            case BITS1     : return 0;
            case BITS2     : return 0;
            case BITS4     : return 0;
            case INT8      : return Byte.MIN_VALUE;
            case UINT8     : return 0;
            case INT16     : return Short.MIN_VALUE;
            case UINT16    : return 0;
            case INT24     : return -0x7FFFFF;
            case UINT24    : return 0;
            case INT32       : return Integer.MIN_VALUE;
            case UINT32      : return 0;
            case INT64      : return Long.MIN_VALUE;
            case UINT64     : return Long.MIN_VALUE;
            case FLOAT32     : return -Float.MAX_VALUE;
            case FLOAT64    : return -Double.MAX_VALUE;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public Number getMaxValue() {
        switch(type){
            case BITS1     : return 1;
            case BITS2     : return 3;
            case BITS4     : return 7;
            case INT8      : return Byte.MAX_VALUE;
            case UINT8     : return 0xFF;
            case INT16     : return Short.MAX_VALUE;
            case UINT16    : return 0xFFFF;
            case INT24     : return 0x7FFFFF;
            case UINT24    : return 0xFFFFFF;
            case INT32       : return Integer.MAX_VALUE;
            case UINT32      : return 0xFFFFFFFFl;
            case INT64      : return Long.MAX_VALUE;
            case UINT64     : return Long.MAX_VALUE;
            case FLOAT32     : return Float.MAX_VALUE;
            case FLOAT64    : return Double.MAX_VALUE;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public byte[] encode(Number value) throws IOException {
        byte[] data = new byte[8];
        switch(type){
//            case BITS1     : candidate = ds.readBits(1); break;
//            case BITS2     : candidate = ds.readBits(2); break;
//            case BITS4     : candidate = ds.readBits(4); break;
            case INT8      : enc.writeByte(value.byteValue(), data, 0); break;
            case UINT8     : enc.writeUByte(value.intValue(), data, 0); break;
            case INT16     : enc.writeShort(value.shortValue(), data, 0); break;
            case UINT16    : enc.writeUShort(value.intValue(), data, 0); break;
            case INT24     : enc.writeInt24(value.intValue(), data, 0); break;
            case UINT24    : enc.writeUInt24(value.intValue(), data, 0); break;
            case INT32       : enc.writeInt(value.intValue(), data, 0); break;
            case UINT32      : enc.writeInt(value.intValue(), data, 0); break;
            case INT64      : enc.writeLong(value.longValue(), data, 0); break;
            case UINT64     : enc.writeLong(value.longValue(), data, 0); break;
            case FLOAT32     : enc.writeFloat(value.floatValue(), data, 0); break;
            case FLOAT64    : enc.writeDouble(value.doubleValue(), data, 0); break;
        }
        return Arrays.copy(data, 0, getBitSize()/8);
    }

}