
package science.unlicense.project.editbit.model;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.encoding.api.io.IOException;

/**
 *
 * @author Johann Sorel
 */
public class CharEncoding extends PrimitiveEncoding{

    public CharEncoding() {
        super(Primitive.UINT8, Endianness.BIG_ENDIAN);
    }

    @Override
    public Chars getName() {
        return new Chars("Char");
    }

    @Override
    public Object decode(byte[] buffer, int offset) throws IOException {
        Number n = (Number) super.decode(buffer,offset);
        return (char)n.intValue();
    }

}
