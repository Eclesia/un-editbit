
package science.unlicense.project.editbit.model;

import java.util.ArrayList;
import java.util.List;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.primitive.ByteSequence;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.number.Primitive;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.math.impl.diff.DiffEngine;

/**
 *
 * @author Johann Sorel
 */
public class Constants {


    public static final List<DataEncoding> DATA_ENC = new ArrayList<DataEncoding>();
    static {
        DATA_ENC.add(new BitsEncoding());
        DATA_ENC.add(new HexEncoding());
        DATA_ENC.add(new CharEncoding());

        DATA_ENC.add(new PrimitiveEncoding(Primitive.INT8, Endianness.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.UINT8, Endianness.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.INT16, Endianness.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.UINT16, Endianness.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.INT24, Endianness.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.UINT24, Endianness.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.INT32, Endianness.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.UINT32, Endianness.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.INT64, Endianness.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.UINT64, Endianness.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.FLOAT32, Endianness.BIG_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.FLOAT64, Endianness.BIG_ENDIAN));

        DATA_ENC.add(new PrimitiveEncoding(Primitive.INT16, Endianness.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.UINT16, Endianness.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.INT24, Endianness.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.UINT24, Endianness.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.INT32, Endianness.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.UINT32, Endianness.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.INT64, Endianness.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.UINT64, Endianness.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.FLOAT32, Endianness.LITTLE_ENDIAN));
        DATA_ENC.add(new PrimitiveEncoding(Primitive.FLOAT64, Endianness.LITTLE_ENDIAN));
    }

    public static Sequence makeDiff(Path file1, Path file2) throws IOException{
        ByteInputStream is = file1.createInputStream();
        final byte[] file1Bytes = IOUtilities.readAll(is);
        is.dispose();
        is = file2.createInputStream();
        final byte[] file2Bytes = IOUtilities.readAll(is);
        is.dispose();

        final DiffEngine engine = new DiffEngine();
        return engine.processDiff(new ByteSequence(file1Bytes), new ByteSequence(file2Bytes));
    }

}
