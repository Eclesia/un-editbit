
package science.unlicense.project.editbit.analyzer;

import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.concurrent.api.Formats;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.SeekableByteBuffer;
import science.unlicense.encoding.api.io.SeekableInputStream;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.image.api.ImageFormat;
import science.unlicense.math.api.Maths;

/**
 *
 * @author Johann Sorel
 */
public class FileAnalyzer {

    private static final int fileStep = 1024000;
    private static final int bufferSize = 10000;

    public static void main(String[] args) throws IOException {

        final Path path = Paths.resolve(new Chars("file:/..."));


        final Format[] formats = Formats.getFormats();


        //find max signature length
        int maxLength = 0;
        for(int i=0;i<formats.length;i++){
            System.out.println(formats[i].getIdentifier());

            final Sequence signatures = formats[i].getSignature();
            for(int k=0;k<signatures.getSize();k++){
                final byte[] sign = (byte[]) signatures.get(k);
                maxLength = Maths.max(maxLength, sign.length);
            }
        }
        final byte[] signature = new byte[maxLength];

        for(int i=0;i<formats.length;i++){
            if(formats[i].getIdentifier().toString().equals("pcx")
               || formats[i].getIdentifier().toString().equals("sgi")
               || formats[i].getIdentifier().toString().equals("ico")
               || formats[i].getIdentifier().toString().equals("apng")
               || formats[i].getIdentifier().toString().equals("bmp")
               || formats[i].getIdentifier().toString().equals("cur")
               || formats[i].getIdentifier().toString().equals("ani")
               || formats[i].getIdentifier().toString().equals("jpeg")
               || formats[i].getIdentifier().toString().equals("netcdf")
               || formats[i].getIdentifier().toString().equals("pnm")){
                formats[i] = null;
            }
        }


        final SeekableByteBuffer sb = path.createSeekableBuffer(true, false, false);
        final DataInputStream ds = new DataInputStream(new SeekableInputStream(sb));
        long fileOffset = 0;

        loop:
        for(long k=0;;k++){
            sb.setPosition(k);
//            if(k>0){
//                long nb = (k-fileOffset) / fileStep;
//                bs.skip(nb*fileStep);
//                bs.mark();
//                fileOffset += nb*fileStep;
//                k -= nb*fileStep;
//                bs.skip(k);
//
////                while ( (k-fileOffset)>fileStep) {
////                    //don't keep to much data backward
////                    fileOffset += fileStep;
////                    bs.skip(fileOffset);
////                    bs.mark();
////                    System.out.println(k);
////                }
//            }


            ds.readFully(signature);
            sb.setPosition(k);


            for(int i=0;i<formats.length;i++){
                if(formats[i]==null) continue;
                final Sequence signatures = formats[i].getSignature();
                for(int l=0;l<signatures.getSize();l++){
                    final byte[] sign = (byte[]) signatures.get(i);
                    if(Arrays.equals(sign,0,sign.length,signature,0)){
                        //if(formats[i].getIdentifier().toJVMString().equals("pcx")){
                            System.out.println(k +"  "+formats[i].getIdentifier());
                        //}

                        //try to read and extract
                            if(formats[i] instanceof ImageFormat){

//                                final ImageReader reader = ((ImageFormat)formats[i]).createReader();
//                                reader.setInput(ds);
//                                final long o = k;
//                                final AtomicBoolean bool = new AtomicBoolean(false);
//                                final Thread t = new Thread(){
//                                    @Override
//                                    public void run() {
//                                        try {
//                                            Image img = reader.read(null);
//                                            Images.write(img, new Chars("bmp"), Paths.resolve(new Chars("file:/...")));
//                                            bool.set(true);
//                                        }catch(Exception ex) {
//                                            System.out.println(ex.getMessage());
//                                        }
//                                    }
//                                };
//                                try {
//                                    t.start();
//                                    t.join(10000);
//                                    t.stop();
//                                    if(bool.get()){
//                                        k = sb.getPosition()-2;
//                                    }
//                                } catch (InterruptedException ex) {
//                                    System.err.println(ex.getMessage());
//                                }
//                                t.stop();

                            }

                    }
                }
            }
        }



    }

}
