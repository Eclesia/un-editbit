
package science.unlicense.project.editbit.stream;

import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.DisplacementLayout;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.display.api.layout.LineLayout;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.MessagePredicate;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.SeekableByteBuffer;
import science.unlicense.math.api.Maths;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.component.path.WPathField;
import science.unlicense.engine.ui.ievent.ActionMessage;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WProgressBar;
import science.unlicense.engine.ui.widget.WScrollContainer;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.swing.SwingFrameManager;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.project.editbit.model.BitsEncoding;
import science.unlicense.project.editbit.model.Constants;
import science.unlicense.project.editbit.model.DataEncoding;

/**
 *
 * @author Johann Sorel
 */
public class WStreamView extends WContainer {

    public static final Chars PROPERTY_PATH = new Chars("Path");

    private final WSpinner guiBitOffset = new WSpinner(new NumberSpinnerModel(Integer.class, 0, 0, Integer.MAX_VALUE, 1));
    private final WContainer guiGrid = new WContainer(new GridLayout(0, 1));
    private final WSpinner guiOffset = new WSpinner(new NumberSpinnerModel(Long.class, 0, 0, Long.MAX_VALUE, 1));
    private final WPathField guiPath = new WPathField();
    private final WLabel lblByte = new WLabel(new Chars("Byte offset"));
    private final WLabel lblBit = new WLabel(new Chars("Bit offset"));
    private final WLabel lblFile = new WLabel(new Chars("File"));
    private final WContainer pane = new WContainer(new BorderLayout());
    private final WScrollContainer scrollpane = new WScrollContainer(pane);
    private final WButtonBar toolbar = new WButtonBar();
    private final WProgressBar filePosition = new WProgressBar();

    //search
    private final WSpinner guiSearchValue = new WSpinner(new NumberSpinnerModel(Double.class, 0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 1));
    private final WButton guiSearch = new WButton(new Chars("Search"));

    private final Sequence bands = new ArraySequence();

    /**
     * Creates new form JStreamView
     */
    public WStreamView() {
        setLayout(new BorderLayout());
        toolbar.setLayout(new LineLayout());

        guiPath.setPreviewText(new Chars("select file . . ."));
        guiPath.varPath().sync(varPath());
        guiPath.varPath().addListener(new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                Path path = (Path) ((PropertyMessage)event.getMessage()).getNewValue();
                WStreamView.this.setPropertyValue(PROPERTY_PATH, path);
                updateData();
            }
        });

        guiOffset.setOverrideExtents(new Extents(120, 22));
        guiOffset.varValue().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                updateData();
            }
        });

        guiBitOffset.setOverrideExtents(new Extents(80, 22));
        guiBitOffset.varValue().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                updateData();
            }
        });

        guiSearchValue.setOverrideExtents(new Extents(120, 22));
        guiSearch.addEventListener(ActionMessage.PREDICATE,new EventListener() {
            public void receiveEvent(Event event) {
                search();
            }
        });
        pane.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final MouseMessage message = (MouseMessage) event.getMessage();
                if (message.isConsumed()) return;
                if (message.getType()==MouseMessage.TYPE_WHEEL) {
                    double r = -message.getWheelOffset();
                    long offset = ((Number)guiOffset.getValue()).longValue();
                    if (offset+r>=0) {
                        setByteOffset(offset+r);
                    }
                    message.consume();
                }
            }
        });

        toolbar.addChild(lblFile,null);
        toolbar.addChild(guiPath,null);
        toolbar.addChild(lblByte,null);
        toolbar.addChild(guiOffset,null);
        toolbar.addChild(lblBit,null);
        toolbar.addChild(guiBitOffset,null);
        //jToolBar1.addChild(guiSearchValue,null);
        //jToolBar1.addChild(guiSearch,null);

        addChild(toolbar, BorderConstraint.TOP);
        pane.addChild(guiGrid, BorderConstraint.TOP);
        addChild(scrollpane, BorderConstraint.CENTER);
        addChild(filePosition, BorderConstraint.BOTTOM);
        final DisplacementLayout dispLayout = (DisplacementLayout) scrollpane.getScrollingContainer().getLayout();
        dispLayout.setFillWidth(true);


        for(DataEncoding de : Constants.DATA_ENC){
            final WStreamBand band = new WStreamBand(de);
            bands.add(band);
            band.addEventListener(new MessagePredicate(WStreamBand.WriteEvt.class), new EventListener() {
                @Override
                public void receiveEvent(Event event) {
                    System.out.println("event");
                    final Path path = getPath();
                    try {
                        final WStreamBand.WriteEvt we = (WStreamBand.WriteEvt) event.getMessage();
                        final SeekableByteBuffer sk = path.createSeekableBuffer(true, true, false);
                        sk.setPosition(((Number)guiOffset.getValue()).longValue()+we.offset);
                        sk.write(we.data);
                        sk.flush();
                        sk.dispose();

                    }catch(IOException ex) {
                        ex.printStackTrace();
                    }
                }
            });
        }

        guiGrid.getChildren().addAll(bands);

        updateData();

    }

    public void setByteOffset(Number offset) {
        guiOffset.setValue(offset.longValue());
    }

    public Path getPath() {
        return (Path) getPropertyValue(PROPERTY_PATH);
    }

    public void setPath(Path path) {
        setPropertyValue(PROPERTY_PATH, path);
        updateData();
    }

    public Property varPath() {
        return getProperty(PROPERTY_PATH);
    }

    private void updateData(){

        final Path path = getPath();
        if (path != null) {
            final long byteOffset = (Long)guiOffset.getValue();
            final int bitOffset = ((Number)guiBitOffset.getValue()).intValue();

            try {
                Number size = (Number) path.getPathInfo(Path.INFO_OCTETSIZE);
                if (size!=null) {
                    filePosition.setProgress(Maths.clamp((double)byteOffset/size.longValue(),0.0,1.0) );
                    filePosition.setText(new Chars(""+byteOffset+" / "+size));
                }


                final DataInputStream ds = new DataInputStream(path.createInputStream());
                ds.skipFully(byteOffset);
                ds.readBits(bitOffset);

                BacktrackInputStream bs = new BacktrackInputStream(ds);
                bs.mark();

                for(int i=0;i<bands.getSize();i++){
                    ((WStreamBand)bands.get(i)).setInput(bs);
                }

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

    private void search(){
        final UIFrame frame = SwingFrameManager.INSTANCE.createFrame(false);
        frame.setTitle(new Chars("Search result"));
        final WContainer pane = frame.getContainer();
        pane.setLayout(new GridLayout(Constants.DATA_ENC.size(), 1));
        frame.setSize(800, 600);
        frame.setVisible(true);

        final double searched = ((Number)guiSearchValue.getValue()).doubleValue();

        for(int i=0;i<Constants.DATA_ENC.size();i++){
            final DataEncoding de = Constants.DATA_ENC.get(i);
            if(de instanceof BitsEncoding) continue;
            long maxVal = (1L << de.getBitSize()+1)-1;
            long minVal = -maxVal;
            if(de.getBitSize()<32 && (searched>maxVal || searched<minVal)) continue;

            new Thread(){
                public void run() {
                    System.out.println("Searching as : "+de.getName());

                    final CharBuffer sb = new CharBuffer();
                    try {
                        final BacktrackInputStream bs = new BacktrackInputStream(getPath().createInputStream());
                        final DataInputStream ds = new DataInputStream(bs);
                        final int step = 1024000;
                        long offset = 0;
                        for(long k=0;;k++){
                            bs.rewind();
                            bs.skip(k-offset);
                            if(k>0 && k%step==0){
                                //don't keep to much data backward
                                offset += step;
                                bs.mark();
                                System.out.println(k);
                            }

                            if(de.read(ds).doubleValue() == searched){
                                sb.append(Long.toString(k));
                                sb.append(',');
                            }
                        }

//                        final SeekableByteBuffer ss = file.createSeekableBuffer(true, false, false);
//                        final byte[] buffer = new byte[(de.getBitSize()+7)/8];
//                        for(long k=0,n=ss.getSize(); k<n; k++){
//                            ss.setPosition(k);
//                            ss.read(buffer);
//                            Number dd = (Number) de.decode(buffer, 0);
//                            if(dd.doubleValue() == searched){
//                                offset = ss.getPosition() - (de.getBitSize()/8);
//                                sb.append(offset);
//                                sb.append(',');
//                            }
//                        }


//                        final DataInputStream ds = new DataInputStream(file.createInputStream());
//                        for(;;){
//                            if(de.read(ds).doubleValue() == searched){
//                                offset = ds.getByteOffset() - (de.getBitSize()/8);
//                                sb.append(offset);
//                                sb.append(',');
//                            }
//                        }
                    } catch (Exception ex) {
                        // end of file or error, we don't care

                    }


                    final WContainer sub = new WContainer(new BorderLayout());
                    sub.addChild(new WLabel(de.getName()), BorderConstraint.LEFT);
                    final WLabel offsets = new WLabel(sb.toChars());
                    sub.addChild(offsets, BorderConstraint.CENTER);

                    pane.addChild(sub,null);
                }
            }.start();
        }


    }

}