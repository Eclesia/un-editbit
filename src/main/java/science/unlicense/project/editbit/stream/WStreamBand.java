
package science.unlicense.project.editbit.stream;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.display.api.font.FontStore;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.layout.StackConstraint;
import science.unlicense.display.api.layout.StackLayout;
import science.unlicense.display.api.painter2d.Paint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.encoding.api.io.DataInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.impl.io.BacktrackInputStream;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.project.editbit.model.BitsEncoding;
import science.unlicense.project.editbit.model.DataEncoding;
import science.unlicense.project.editbit.model.PrimitiveEncoding;

/**
 *
 * @author Johann Sorel
 */
public class WStreamBand extends WContainer{

    public static final Chars PROPERTY_EDITABLE = new Chars("Editable");
    private static final int BYTE_WIDTH = 30;
    protected static final Dictionary CLASS_DEFAULTS;
    static {
        final Dictionary defs = new HashDictionary();
        defs.addAll(WContainer.CLASS_DEFAULTS);
        defs.add(PROPERTY_EDITABLE, Boolean.FALSE);
        CLASS_DEFAULTS = Collections.readOnlyDictionary(defs);
    }

    private BacktrackInputStream bs;
    private DataInputStream ds;
    private final DataEncoding enc;
    private WCells cells;

    public WStreamBand(final DataEncoding enc) {
        super(new BorderLayout());
        this.enc = enc;

        final WLabel lbl = new WLabel(enc.getName());
        lbl.getStyle().getSelfRule().setProperties(new Chars("background:{\nfill-paint:@color-background\n}\ngraphic:{\nbrush:plainbrush(1 'round')\nbrush-paint:@color-delimiter\n}"));
        lbl.setOverrideExtents(new Extents(90, 24));
        lbl.setHorizontalAlignment(WLabel.HALIGN_CENTER);

        if(enc.getClass().equals(PrimitiveEncoding.class)) {
            cells = new WCells();
            addChild(cells,BorderConstraint.CENTER);
        } else {
            addChild(new WPaint(),BorderConstraint.CENTER);
        }
        addChild(lbl,BorderConstraint.LEFT);

    }

    @Override
    protected Dictionary getClassDefaultProperties() {
        return CLASS_DEFAULTS;
    }

    public void setInput(BacktrackInputStream bs) {
        this.bs = bs;
        this.ds = new DataInputStream(bs);
        if(cells!=null) cells.update();
    }

    public boolean isEditable() {
        return (Boolean)getPropertyValue(PROPERTY_EDITABLE);
    }

    public void setEditable(boolean editable) {
        setPropertyValue(PROPERTY_EDITABLE, editable);
    }

    private class WCells extends WContainer {

        public WCells() {
            setOverrideExtents(new Extents(0, 0, 5, 5, Double.MAX_VALUE,Double.MAX_VALUE));
            final FormLayout layout = new FormLayout();
            setLayout(layout);
        }

        @Override
        public void setEffectiveExtent(Extent extent) {
            super.setEffectiveExtent(extent);

            final int sizeInByte = enc.getBitSize() / 8 ;
            final int blockWidth = sizeInByte * BYTE_WIDTH;

            double width = extent.get(0);
            final FormLayout layout = (FormLayout) getLayout();
            layout.setRowSize(0, FormLayout.SIZE_EXPAND);

            int x = 0;
            final Sequence newChildren = new ArraySequence();
            while (width>0) {
                layout.setColumnSize(x, blockWidth);
                WPrimitiveCell cell = new WPrimitiveCell(x*sizeInByte);
                cell.setLayoutConstraint(new FillConstraint.Builder().coord(x, 0).fill().build());
                newChildren.add(cell);
                cell.update();
                width -= blockWidth;
                x++;
            }
            getChildren().replaceAll(newChildren);
        }

        public void update() {
            for (int x=0;x<getChildren().getSize();x++) {
                ((WPrimitiveCell)getChildNode(x)).update();
            }
        }

    }


    private class WPrimitiveCell extends WContainer {

        private final WLabel lbl = new WLabel();
        private WSpinner tf;
        private final int offset;

        private final EventListener up = new EventListener() {
                @Override
                public void receiveEvent(Event event) {
                    try {
                        byte[] data = enc.encode((Number)tf.varValue().getValue());
                        WStreamBand.this.getEventManager(true).sendEvent(new Event(WStreamBand.this, new WriteEvt(offset, data)));
                    }catch(Exception ex) {}
                }
            };

        public WPrimitiveCell(final int offset) {
            super(new StackLayout());
            this.offset = offset;
            lbl.setHorizontalAlignment(WLabel.HALIGN_CENTER);
            lbl.getStyle().getSelfRule().setProperties(new Chars("margin:2"));
            addChild(lbl, new StackConstraint(1));
            getStyle().getSelfRule().setProperties(new Chars("graphic:{\nbrush:plainbrush(1 'round')\nbrush-paint:@color-delimiter\n}"));

            WPrimitiveCell.this.varMouseOver().addListener(new EventListener() {
                public void receiveEvent(Event event) {
                    if (tf==null && isMouseOver() && isEditable()) {
                        tf = new WSpinner(new NumberSpinnerModel(enc.getValueClass(), 0, enc.getMinValue(), enc.getMaxValue(), 1));
                        addChild(tf, new StackConstraint(2));
                        tf.varVisible().sync(WPrimitiveCell.this.varMouseOver(),true);
                        update();
                    }
                }
            });
        }

        private void update() {
            if(bs==null) return;
            Chars text = Chars.EMPTY;
            bs.rewind();
            try {
                ds.skipFully(offset);
                Number v = enc.read(ds);
                text = new Chars("" + v);
                if (tf!=null) {
                    tf.varValue().removeListener(up);
                    tf.setValue(v);
                    tf.varValue().addListener(up);
                }
            }catch (IOException ex){
            }
            lbl.setText(text);
        }

    }

    private class WPaint extends WLeaf{

        public WPaint() {
            setView(new PaintView(WPaint.this));
        }

        private class PaintView extends WidgetView {

            public PaintView(Widget widget) {
                super(widget);
            }

            @Override
            protected void renderSelf(Painter2D painter, BBox innerBBox) {
                super.renderSelf(painter, innerBBox);

                final Extent ext = getEffectiveExtent();
                final double height = ext.get(1);
                final double width = ext.get(0);
                final double minx = innerBBox.getMin(0);
                final double miny = innerBBox.getMin(1);

                int bx = 0;
                int gx = bx*BYTE_WIDTH;
                final int blockWidth = enc.getBitSize()/8*BYTE_WIDTH;

                final Paint textColor = WidgetStyles.asPaint(widget.getStyle().getPropertyValue(SystemStyle.COLOR_TEXT));
                final Paint mainColor = WidgetStyles.asPaint(widget.getStyle().getPropertyValue(SystemStyle.COLOR_MAIN));
                final Paint delimColor = WidgetStyles.asPaint(widget.getStyle().getPropertyValue(SystemStyle.COLOR_DELIMITER));

                final FontChoice font = new FontChoice(new Chars[]{new Chars("Tuffy")}, 12, FontChoice.WEIGHT_NONE);
                painter.setFont(font);
                painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));
                painter.setPaint(delimColor);
                painter.stroke(new DefaultLine(minx+0, miny+0, minx+width, miny+0));
                painter.setPaint(mainColor);

                final int sizeInByte = (enc.getBitSize()/8);
                final FontStore fontStore = getFrame().getPainter().getFontStore();
                final FontMetadata metadata = fontStore.getFont(font).getMetaData();

                if (ds!=null) bs.rewind();

                while(gx<width){
                    painter.setPaint(delimColor);
                    painter.stroke(new DefaultLine(minx+gx, miny+0, minx+gx, miny+height));

                    if(enc instanceof BitsEncoding){
                        painter.setPaint(delimColor);
                        painter.stroke(new DefaultLine(minx+gx, miny+height/2, minx+gx+blockWidth, miny+height/2));
                        painter.stroke(new DefaultLine(minx+gx+(1*blockWidth)/4, miny+0, minx+gx+(1*blockWidth)/4, miny+height));
                        painter.stroke(new DefaultLine(minx+gx+(2*blockWidth)/4, miny+0, minx+gx+(2*blockWidth)/4, miny+height));
                        painter.stroke(new DefaultLine(minx+gx+(3*blockWidth)/4, miny+0, minx+gx+(3*blockWidth)/4, miny+height));
                        byte b = 0;
                        try {
                            if (ds!=null) b = ds.readByte();
                        } catch (IOException ex) {
                            break;
                        }
                        painter.setPaint(mainColor);
                        if( (b & 0x80) !=0 ) painter.fill(new Rectangle(minx+gx+(0*blockWidth)/4, miny+0, blockWidth/4+1, height/2));
                        if( (b & 0x40) !=0 ) painter.fill(new Rectangle(minx+gx+(1*blockWidth)/4, miny+0, blockWidth/4+1, height/2));
                        if( (b & 0x20) !=0 ) painter.fill(new Rectangle(minx+gx+(2*blockWidth)/4, miny+0, blockWidth/4+1, height/2));
                        if( (b & 0x10) !=0 ) painter.fill(new Rectangle(minx+gx+(3*blockWidth)/4, miny+0, blockWidth/4+1, height/2));
                        if( (b & 0x08) !=0 ) painter.fill(new Rectangle(minx+gx+(0*blockWidth)/4, miny+height/2, blockWidth/4+1, height/2));
                        if( (b & 0x04) !=0 ) painter.fill(new Rectangle(minx+gx+(1*blockWidth)/4, miny+height/2, blockWidth/4+1, height/2));
                        if( (b & 0x02) !=0 ) painter.fill(new Rectangle(minx+gx+(2*blockWidth)/4, miny+height/2, blockWidth/4+1, height/2));
                        if( (b & 0x01) !=0 ) painter.fill(new Rectangle(minx+gx+(3*blockWidth)/4, miny+height/2, blockWidth/4+1, height/2));
                        painter.setPaint(delimColor);
                        painter.stroke(new DefaultLine(minx+gx, miny+0, minx+gx+blockWidth, miny+0));
                        painter.stroke(new DefaultLine(minx+gx, miny+height, minx+gx+blockWidth, miny+height));

                    }else{
                        Chars text = Chars.EMPTY;
                        try {
                            if (ds!=null) text = new Chars(""+enc.decode(ds));
                        } catch (IOException ex) {
                        }

                        final BBox textExtent = metadata.getCharsBox(text);

                        double cy = (height-textExtent.getSpan(1))/2.0
                                  + miny
                                  +metadata.getAscent();

                        painter.setPaint(textColor);
                        painter.fill(text, (float)minx+gx+2, (float) cy);
                    }

                    bx+= sizeInByte;
                    gx = bx*BYTE_WIDTH;
                }
            }
        }
    }

    public static class WriteEvt implements EventMessage {

        public int offset;
        public byte[] data;

        public WriteEvt(int offset, byte[] data) {
            this.offset = offset;
            this.data = data;
        }

        @Override
        public boolean isConsumable() {
            return false;
        }

        @Override
        public boolean isConsumed() {
            return false;
        }

        @Override
        public void consume() {
        }

    }

}
