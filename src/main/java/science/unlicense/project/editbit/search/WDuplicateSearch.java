package science.unlicense.project.editbit.search;


import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.common.api.Orderable;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.component.path.WPathField;
import science.unlicense.engine.ui.ievent.ActionMessage;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.model.RowModel;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WTable;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.encoding.impl.cryptography.hash.CRC32;
import science.unlicense.encoding.impl.cryptography.hash.HashFunction;
import science.unlicense.encoding.impl.cryptography.hash.HashInputStream;

/**
 *
 * @author Johann Sorel
 */
public class WDuplicateSearch extends WContainer{

    private final WLabel searchPathLbl = new WLabel(new Chars("Path :"));
    private final WPathField searchPath = new WPathField();
    private final WButton search = new WButton(new Chars("Search"));
    private final WButton delete = new WButton(new Chars("Delete"));
    private final WTable list = new WTable();
    private final Sequence matches = new ArraySequence();

    public WDuplicateSearch(){
        super(new BorderLayout());

        delete.setEnable(false);
        
        final FormLayout fl = new FormLayout();
        fl.setColumnSize(1, FormLayout.SIZE_EXPAND);
        fl.setDefaultColumnSpace(4);
        final WContainer pane = new WContainer(fl);
        pane.getStyle().getSelfRule().setProperties(new Chars("margin:8"));
        
        pane.addChild(searchPathLbl,    FillConstraint.builder().coord(0,0).build());
        pane.addChild(searchPath,       FillConstraint.builder().coord(1,0).fill(true, false).build());
        pane.addChild(search,           FillConstraint.builder().coord(2,0).fill(false, false).build());
        pane.addChild(delete,           FillConstraint.builder().coord(3,0).fill(false, false).build());

        final DefaultColumn col1 = new DefaultColumn(Chars.EMPTY, new ObjectPresenter() {
            @Override
            public Widget createWidget(Object candidate) {
                WTextField tf = new WTextField(((Path[])candidate)[0].toURI());
                tf.setEnable(false);
                return tf;
            }
        });
        col1.setBestWidth(FormLayout.SIZE_EXPAND);

        final DefaultColumn col2 = new DefaultColumn(Chars.EMPTY, new ObjectPresenter() {
            @Override
            public Widget createWidget(Object candidate) {
                WTextField tf = new WTextField(((Path[])candidate)[1].toURI());
                tf.setEnable(false);
                return tf;
            }
        });
        col2.setBestWidth(FormLayout.SIZE_EXPAND);
        list.getColumns().add(col1);
        list.getColumns().add(col2);

        addChild(pane, BorderConstraint.TOP);
        addChild(list, BorderConstraint.CENTER);
        
        RowModel model = new DefaultRowModel(matches);
        list.setRowModel(model);

        search.addEventListener(ActionMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                search.setEnable(false);
                new Thread() {
                    @Override
                    public void run() {
                        search();
                        search.setEnable(true);
                        delete.setEnable(true);
                    }
                }.start();
            }
        });
        
        delete.addEventListener(ActionMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                for(int i=0;i<matches.getSize();i++) {
                    Path[] m = (Path[]) matches.get(i);
                    try {
                        m[1].delete();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
        
    }
    
    private void search() {
        //list and create checksum for all files
        final Path path = searchPath.getPath();
        final Sequence files = new ArraySequence();
        try {
            search(files,path);
            Collections.sort(files);

            //check all files with same checksum
            final Sequence sameChecksums = new ArraySequence();
            CheckPath p1, p2;
            for(int i=0,n=files.getSize()-1;i<n;i++){
                p1 = (CheckPath) files.get(i);
                p2 = (CheckPath) files.get(i+1);
                if(p1.checksum==p2.checksum){
                    if(sameChecksums.isEmpty()){
                        sameChecksums.add(p1);
                    }
                    sameChecksums.add(p2);
                }else{
                    compare(sameChecksums);
                    sameChecksums.removeAll();
                }
            }
            compare(sameChecksums);
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    private void compare(Sequence files) throws IOException{
        for(int i=0;i<files.getSize();i++){
            final Path p1 = ((CheckPath)files.get(i)).path;
            for(int k=files.getSize()-1;k>i;k--){
                final Path p2 = ((CheckPath)files.get(k)).path;
                if(compare(p1, p2)){
                    matches.add(new Path[]{p1,p2});
//                    files.remove(k);
                }
            }
        }
    }

    private static boolean compare(Path p1, Path p2) throws IOException{
        ByteInputStream in1 = p1.createInputStream();
        ByteInputStream in2 = p2.createInputStream();

        int i1,i2;
        for(;;){
            i1 = in1.read();
            i2 = in2.read();
            if(i1!=i2){
                //different files
                return false;
            }else if(i1==-1){
                break;
            }
        }
        in1.dispose();
        in2.dispose();
        return true;
    }

    private static void search(Sequence files, Path path) throws IOException{
        if(path.isContainer()){
            final Iterator ite = path.getChildren().createIterator();
            while(ite.hasNext()) {
                search(files, (Path)ite.next());
            }
        }else{
            files.add(new CheckPath(path));
            if(files.getSize()%1000==0) System.out.println(files.getSize());
        }
    }

    private static final class CheckPath implements Orderable{
        private final int checksum;
        private final Path path;

        public CheckPath(Path path) throws IOException {
            this.path = path;
            ByteInputStream in = path.createInputStream();
            final HashFunction hf = new CRC32();
            in = new HashInputStream(hf, in);
            
            //limit to the first 20Ko
            int nb = 20000;
            for(long k=0;k>=0 && nb>0;k=in.skip(nb),nb-=k);
            in.dispose();
            checksum = (int) hf.getResultLong();
        }

        @Override
        public int order(Object other) {
            return checksum - ((CheckPath)other).checksum;
        }

    }

}