
package science.unlicense.project.editbit.search;

import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.layout.SplitConstraint;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.AbstractIterator;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.common.api.regex.Regex;
import science.unlicense.common.api.regex.RegexExec;
import science.unlicense.engine.ui.component.path.WPathField;
import science.unlicense.engine.ui.ievent.ActionMessage;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WCheckBox;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSplitContainer;
import science.unlicense.engine.ui.widget.WTable;
import science.unlicense.engine.ui.widget.WTextField;

/**
 * Tool to search folders.
 *
 * @author Johann Sorel
 */
public class WFileSearchPane extends WContainer{

    private final WLabel searchPathLbl = new WLabel(new Chars("Path :"));
    private final WPathField searchPath = new WPathField();
    private final WLabel searchPatternLbl = new WLabel(new Chars("Regex :"));
    private final WTextField searchPattern = new WTextField(Chars.EMPTY);
    private final WCheckBox searchSubFolder = new WCheckBox(new Chars("Search sub-folders"));
    private final WButton search = new WButton(new Chars("Search"));
    private final WTable list = new WTable();

    public WFileSearchPane(){
        super(new BorderLayout());
        final WSplitContainer split = new WSplitContainer();
        addChild(split, BorderConstraint.CENTER);

        final WContainer pane = new WContainer(new FormLayout());
        ((FormLayout)pane.getLayout()).setColumnSize(1, FormLayout.SIZE_EXPAND);
        pane.addChild(searchPathLbl,    FillConstraint.builder().coord(0,0).build());
        pane.addChild(searchPath,       FillConstraint.builder().coord(1,0).fill(true, false).build());
        pane.addChild(searchPatternLbl, FillConstraint.builder().coord(0,1).build());
        pane.addChild(searchPattern,    FillConstraint.builder().coord(1,1).fill(true, false).build());
        pane.addChild(searchSubFolder,  FillConstraint.builder().coord(0,2).span(2,1).build());
        pane.addChild(search,           FillConstraint.builder().coord(0,3).span(2,1).fill(true, false).build());

        final DefaultColumn col = new DefaultColumn();
        col.setBestWidth(FormLayout.SIZE_EXPAND);
        list.getColumns().add(col);

        split.addChild(pane,SplitConstraint.LEFT);
        split.addChild(list,SplitConstraint.RIGHT);

        search.addEventListener(ActionMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                search();
            }
        });
    }
        

    private void search() {

        final Path path = searchPath.getPath();
        final RegexExec regex = Regex.compile(searchPattern.getText().toChars());

        final Predicate predicate = new Predicate() {
            @Override
            public Boolean evaluate(Object candidate) {
                final Path p = (Path) candidate;
                return regex.match(new Chars(p.getName()));
            }
        };

        final Sequence values = new ArraySequence();
        final SearchIterator ite = new SearchIterator(path, predicate, searchSubFolder.isCheck());
        while(ite.hasNext()){
            values.add(ite.next());
        }

        list.setRowModel(new DefaultRowModel(values));
    }


    private static class SearchIterator extends AbstractIterator{

        private final Sequence stack = new ArraySequence();
        private final Predicate predicate;
        private final boolean searchSubContainer;

        private SearchIterator(Path path, Predicate predicate, boolean searchSubContainer){
            stack.add(new State(path));
            this.predicate = predicate;
            this.searchSubContainer = searchSubContainer;
        }

        protected void findNext() {

            int idx;
            while(!stack.isEmpty()){
                idx = stack.getSize()-1;
                State state = (State) stack.get(idx);
                if(!state.ite.hasNext()){
                    //we reached the end of the container, remove it from the stack
                    stack.remove(idx);
                }else{
                    Path child = (Path) state.ite.next();

                    try {
                        if(searchSubContainer && child.isContainer()){
                            stack.add(new State(child));
                        }
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    if(predicate.evaluate(child)){
                        //found a match
                        nextValue = child;
                        break;
                    }
                }
            }
        }

        private static final class State{
            Path path;
            Iterator ite;
            public State(Path path) {
                this.path = path;
                this.ite = path.getChildren().createIterator();
            }
        }

    }

}
