
package science.unlicense.project.editbit.diff;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.impl.diff.DiffEngine;

/**
 *
 * @author Johann Sorel
 */
public class WDiffView extends WLeaf{

    public static final Chars PROPERTY_OFFSET = new Chars("byteOffset");
    public static final int CELL_SIZE = 8;
    public static final ColorPaint DIFF_NOCHANGE = new ColorPaint(Color.BLACK);
    public static final ColorPaint DIFF_REMOVE = new ColorPaint(Color.RED);
    public static final ColorPaint DIFF_ADD = new ColorPaint(Color.GREEN);
    public static final ColorPaint DIFF_REPLACE = new ColorPaint(Color.BLUE);
    public static final ColorPaint TRS = new ColorPaint(Color.TRS_BLACK);


    private final Sequence diffs = new ArraySequence();
    private boolean isMainFile = true;

    public WDiffView(boolean isMainFile) {
        this.isMainFile = isMainFile;
        setView(new DiffView());
    }

    @Override
    protected void receiveEventParent(Event event) {
        super.receiveEventParent(event);
        if (event.getMessage() instanceof MouseMessage) {
           final MouseMessage mm = (MouseMessage) event.getMessage();
           if (mm.getType()==MouseMessage.TYPE_TYPED) {
                int byteOffset = computeBytePosition(mm.getMousePosition().get(0), mm.getMousePosition().get(1));
                if(byteOffset>=0){
                    sendPropertyEvent(PROPERTY_OFFSET, -1, byteOffset);
                }
           }
        }
    }

    public void setDiffs(Sequence diffs){
        this.diffs.replaceAll(diffs);
        setDirty();
    }

    private int computeBytePosition(double mouseX, double mouseY){
        mouseX /= CELL_SIZE;
        mouseY /= CELL_SIZE;

        final double width = getInnerExtent().getSpan(0);
        final int nbPerRow = (int) (width / CELL_SIZE);

        int idxFix = 0;
        int x = 0;
        int y = 0;
        for(int i=0,n=diffs.getSize();i<n;i++){
            final DiffEngine.DiffResultSpan span = (DiffEngine.DiffResultSpan) diffs.get(i);
            final DiffEngine.DiffResultSpanStatus status = span.getStatus();

            int startIdx;
            if(isMainFile){
                startIdx = span.getSourceIndex();
            }else{
                startIdx = span.getDestIndex();
            }
            startIdx += idxFix;
            final int length = span.getLength();

            if(status.equals(DiffEngine.DiffResultSpanStatus.AddDestination)){
                if(isMainFile){
                    idxFix+=length;
                    continue;
                }
            }else if(status.equals(DiffEngine.DiffResultSpanStatus.DeleteSource)){
                if(!isMainFile){
                    idxFix+=length;
                    continue;
                }
            }

            int inc = 0;
            x = startIdx % nbPerRow;
            y = startIdx / nbPerRow;
            for(int k=0;k<length;k++){
                if(x==mouseX && y==mouseY){
                    if(isMainFile){
                        return span.getSourceIndex()+inc;
                    }else{
                        return span.getDestIndex()+inc;
                    }
                }

                inc++;
                x++;
                if(x>=nbPerRow){
                    x=0;
                    y++;
                }
            }
        }

        return -1;
    }

    private final class DiffView extends WidgetView {

        DiffView() {
            super(WDiffView.this);
        }

        @Override
        protected void renderSelf(Painter2D painter, BBox innerBBox) {
            super.renderSelf(painter, innerBBox);


            final double width = innerBBox.getSpan(0);
            final int nbPerRow = (int) (width / CELL_SIZE);

            int idxFix = 0;
            int x = 0;
            int y = 0;
            for(int i=0,n=diffs.getSize();i<n;i++){
                final DiffEngine.DiffResultSpan span = (DiffEngine.DiffResultSpan) diffs.get(i);
                final DiffEngine.DiffResultSpanStatus status = span.getStatus();

                int startIdx;
                if(isMainFile){
                    startIdx = span.getSourceIndex();
                }else{
                    startIdx = span.getDestIndex();
                }
                startIdx += idxFix;
                final int length = span.getLength();

                if(status.equals(DiffEngine.DiffResultSpanStatus.NoChange)){
                    painter.setPaint(DIFF_NOCHANGE);
                }else if(status.equals(DiffEngine.DiffResultSpanStatus.AddDestination)){
                    if(isMainFile){
                        idxFix+=length;
                        painter.setPaint(TRS);
                        continue;
                    }else{
                        painter.setPaint(DIFF_ADD);
                    }
                }else if(status.equals(DiffEngine.DiffResultSpanStatus.DeleteSource)){
                    if(isMainFile){
                        painter.setPaint(DIFF_REMOVE);
                    }else{
                        idxFix+=length;
                        painter.setPaint(TRS);
                        continue;
                    }
                }else if(status.equals(DiffEngine.DiffResultSpanStatus.Replace)){
                    painter.setPaint(DIFF_REPLACE);
                }


                x = startIdx % nbPerRow;
                y = startIdx / nbPerRow;
                for(int k=0;k<length;k++){
                    painter.fill(new Rectangle(x*CELL_SIZE+innerBBox.getMin(0), y*CELL_SIZE+innerBBox.getMin(1), CELL_SIZE-1, CELL_SIZE-1));
                    x++;
                    if(x>=nbPerRow){
                        x=0;
                        y++;
                    }
                }
            }

        }

    }

}
