
package science.unlicense.project.editbit.diff;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.display.api.layout.LineLayout;
import science.unlicense.display.api.layout.SplitConstraint;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WLoader;
import science.unlicense.engine.ui.widget.WSplitContainer;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.project.editbit.model.Constants;
import science.unlicense.project.editbit.stream.WStreamView;

/**
 *
 * @author Johann Sorel
 */
public class WDiffPane extends WContainer{

    private final WStreamView view1 = new WStreamView();
    private final WStreamView view2 = new WStreamView();
    private final WDiffView diff1 = new WDiffView(true);
    private final WDiffView diff2 = new WDiffView(false);

    private final WSplitContainer split = new WSplitContainer(true);
    private final WContainer infoPane = new WContainer(new LineLayout());
    private final WContainer diffsPane = new WContainer(new BorderLayout());
    private final WContainer gridTop = new WContainer(new GridLayout(1,2,4,4));
    private final WContainer gridBottom = new WContainer(new GridLayout(1,2,4,4));

    public WDiffPane() {
        super(new BorderLayout());

        infoPane.addChild(new WLabel(new Chars("No change"),new WGraphicImage(createIcon(WDiffView.DIFF_NOCHANGE))),null);
        infoPane.addChild(new WLabel(new Chars("Added"),new WGraphicImage(createIcon(WDiffView.DIFF_ADD))),null);
        infoPane.addChild(new WLabel(new Chars("Removed"),new WGraphicImage(createIcon(WDiffView.DIFF_REMOVE))),null);
        infoPane.addChild(new WLabel(new Chars("Replaced"),new WGraphicImage(createIcon(WDiffView.DIFF_REPLACE))),null);
        gridTop.addChild(view1,null);
        gridTop.addChild(view2,null);
        gridBottom.addChild(diff1,null);
        gridBottom.addChild(diff2,null);
        diffsPane.addChild(infoPane, BorderConstraint.TOP);
        diffsPane.addChild(gridBottom, BorderConstraint.CENTER);

        split.addChild(gridTop,SplitConstraint.LEFT);
        split.addChild(diffsPane,SplitConstraint.RIGHT);

        addChild(split, BorderConstraint.CENTER);

        final EventListener lst = new EventListener() {
            public void receiveEvent(Event event) {
                updateDiff();
            }
        };
        view1.varPath().addListener(lst);
        view2.varPath().addListener(lst);

        diff1.addEventListener(new PropertyPredicate(WDiffView.PROPERTY_OFFSET), new EventListener() {
            public void receiveEvent(Event event) {
                final int offset = (Integer)((PropertyMessage)event.getMessage()).getNewValue();
                view1.setByteOffset(offset);
            }
        });
        diff2.addEventListener(new PropertyPredicate(WDiffView.PROPERTY_OFFSET), new EventListener() {
            public void receiveEvent(Event event) {
                final int offset = (Integer)((PropertyMessage)event.getMessage()).getNewValue();
                view2.setByteOffset(offset);
            }
        });

    }

    private void updateDiff(){
        final WLoader loader = new WLoader(new Chars("Loading"));
        split.getChildren().remove(diffsPane);
        split.addChild(loader,SplitConstraint.RIGHT);

        Thread t = new Thread() {
            @Override
            public void run() {
                final Path path1 = view1.getPath();
                final Path path2 = view2.getPath();
                if (path1 != null && path2 != null) {
                    try {
                        Sequence diff = Constants.makeDiff(path1, path2);
                        diff1.setDiffs(diff);
                        diff2.setDiffs(diff);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                }
                split.getChildren().remove(loader);
                split.addChild(diffsPane,SplitConstraint.RIGHT);
            }
        };
        t.start();


    }

    private static Image createIcon(ColorPaint color){
        final Image img = Images.create(new Extent.Long(16, 16), Images.IMAGE_TYPE_RGBA);
        img.getColorModel().asTupleBuffer(img).setTuple(new BBox(img.getExtent()), color.getColor());
        return img;
    }

}
