
package science.unlicense.project.editbit;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.MessagePredicate;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.FrameMessage;
import science.unlicense.display.api.layout.AbsoluteLayout;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.display.api.layout.LineLayout;
import science.unlicense.display.api.layout.StackConstraint;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.swing.SwingFrameManager;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.component.path.WPathChooser;
import science.unlicense.engine.ui.component.path.WThumbnail;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.io.RSReader;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.style.WStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.menu.WMenuButton;
import science.unlicense.engine.ui.widget.menu.WMenuDropDown;
import science.unlicense.project.editbit.diff.WDiffPane;
import science.unlicense.project.editbit.search.WDuplicateSearch;
import science.unlicense.project.editbit.search.WFileSearchPane;
import science.unlicense.project.editbit.stream.WStreamView;

/**
 * Main frame.
 *
 * @author Johann Sorel
 */
public class EditBitFrame {

    public static void main(String[] args) {
        new EditBitFrame();
    }

    private final UIFrame frame;
    private final WContainer pane = new WContainer(new BorderLayout());

    public EditBitFrame() {

//        final UIFrame frm = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        frame = SwingFrameManager.INSTANCE.createFrame(false);
        frame.getContainer().setLayout(new BorderLayout());


        final WButtonBar bar = new WButtonBar();
        bar.setLayout(new LineLayout());
        final WMenuDropDown menu = new WMenuDropDown();
        menu.setText(new Chars("File"));

        final WMenuButton createFile = new WMenuButton(new Chars("Create file"),null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            final WPathChooser chooser = new WPathChooser();
                            final Path path = chooser.showSaveDialog(EditBitFrame.this.frame);
                            path.createLeaf();

                            final WStreamView v = new WStreamView();
                            v.setLayoutConstraint(BorderConstraint.CENTER);
                            v.setPath(path);
                            pane.getChildren().replaceAll(new Node[]{v});
                        } catch(IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                }.start();
            }
        });
        final WMenuButton singleFile = new WMenuButton(new Chars("Bit view file"),null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                WStreamView v = new WStreamView();
                v.setLayoutConstraint(BorderConstraint.CENTER);
                pane.getChildren().replaceAll(new Node[]{v});
            }
        });

        final WMenuButton diffFiles = new WMenuButton(new Chars("Diff files"),null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                WDiffPane v = new WDiffPane();
                v.setLayoutConstraint(BorderConstraint.CENTER);
                pane.getChildren().replaceAll(new Node[]{v});
            }
        });

        final WMenuButton searchFiles = new WMenuButton(new Chars("Search files"),null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                WFileSearchPane v = new WFileSearchPane();
                v.setLayoutConstraint(BorderConstraint.CENTER);
                pane.getChildren().replaceAll(new Node[]{v});
            }
        });

        final WMenuButton searchDuplicatesFiles = new WMenuButton(new Chars("Search and remove duplicates"),null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                WDuplicateSearch v = new WDuplicateSearch();
                v.setLayoutConstraint(BorderConstraint.CENTER);
                pane.getChildren().replaceAll(new Node[]{v});
            }
        });
        final WMenuButton exit = new WMenuButton(new Chars("Exit"),null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                System.exit(0);
            }
        });

        menu.getDropdown().addChild(createFile,null);
        menu.getDropdown().addChild(singleFile,null);
        menu.getDropdown().addChild(diffFiles,null);
//        menu.getDropdown().addChild(searchFiles,null);
//        menu.getDropdown().addChild(searchDuplicatesFiles,null);
        menu.getDropdown().addChild(exit,null);
        bar.addChild(menu,null);
        bar.addChild(createThemeButton(SystemStyle.THEME_LIGHT, new Chars(" Light ")), FillConstraint.builder().coord(1, 0).build());
        bar.addChild(createThemeButton(SystemStyle.THEME_DARK, new Chars(" Dark ")), FillConstraint.builder().coord(2, 0).build());
        createThemeButton(SystemStyle.THEME_DARK, new Chars(" Dark ")).doClick();

        final WStartPane startPane = new WStartPane();
        startPane.setLayoutConstraint(BorderConstraint.CENTER);
        pane.getChildren().replaceAll(new Node[]{startPane});

        frame.getContainer().addChild(bar, BorderConstraint.TOP);
        frame.getContainer().addChild(pane, BorderConstraint.CENTER);
        frame.setTitle(new Chars("Unlicense - EditBit"));
        frame.setSize(1280, 800);
        frame.addEventListener(new MessagePredicate(FrameMessage.class), new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                FrameMessage message = (FrameMessage) event.getMessage();
                if (message.getType() == FrameMessage.TYPE_PREDISPOSE) {
                    System.exit(0);
                }
            }
        });
        frame.setVisible(true);

    }

    private static WMenuButton createThemeButton(final Chars path, Chars name){
        final WMenuButton button = new WMenuButton(name,null,new EventListener(){
            @Override
            public void receiveEvent(Event event) {
                    WStyle style = RSReader.readStyle(Paths.resolve(path));
                    for(int i=0;i<style.getRules().getSize();i++){
                        final StyleDocument r = (StyleDocument) style.getRules().get(i);
                        WidgetStyles.mergeDoc(
                            SystemStyle.INSTANCE.getRule(r.getName()),
                            r, false);
                    }

                    SystemStyle.INSTANCE.notifyChanged();
            }
        });
        return button;
    }

    private class WStartPane extends WButtonBar {

        public WStartPane() {
            setLayout(new AbsoluteLayout());

            final WContainer center = new WContainer(new GridLayout(1, 3, 0, 0));

            final WThumbnail thumbnail1 = new WThumbnail(Paths.resolve(new Chars("file:./test.png")), true, null,false,true, Predicate.TRUE);
            thumbnail1.getStyle().getSelfRule().setProperties(new Chars("margin:5"));

            final WButton first = new WButton(new Chars(" Single file "), thumbnail1, new EventListener() {
                @Override
                public void receiveEvent(Event event) {
                    WStreamView v = new WStreamView();
                    v.setLayoutConstraint(BorderConstraint.CENTER);
                    pane.getChildren().replaceAll(new Node[]{v});
                }
            });
            first.setGraphicPlacement(WButton.GRAPHIC_TOP);

            final WThumbnail thumbnail2 = new WThumbnail(Paths.resolve(new Chars("file:./test.png")), true, null,false,true, Predicate.TRUE);
            thumbnail2.getStyle().getSelfRule().setProperties(new Chars("margin:2"));
            final WThumbnail thumbnail3 = new WThumbnail(Paths.resolve(new Chars("file:./test.png")), true, null,false,true, Predicate.TRUE);
            thumbnail3.getStyle().getSelfRule().setProperties(new Chars("margin:2"));
            final WContainer prev = new WContainer(new GridLayout(1, 2));
            prev.addChild(thumbnail2, null);
            prev.addChild(thumbnail3, null);

            final WButton second = new WButton(new Chars(" Compare files "), prev, new EventListener() {
                @Override
                public void receiveEvent(Event event) {
                    WDiffPane v = new WDiffPane();
                    v.setLayoutConstraint(BorderConstraint.CENTER);
                    pane.getChildren().replaceAll(new Node[]{v});
                }
            });
            second.setGraphicPlacement(WButton.GRAPHIC_TOP);

            center.addChild(first, null);
            center.addChild(new WSpace(160,160), null);
            center.addChild(second, null);

            addChild(center, new StackConstraint(0));
        }

    }

}
