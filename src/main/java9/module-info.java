module science.unlicense.p.editbit {
    requires java.desktop;
    requires science.unlicense.format.ttf;
    requires science.unlicense.model2d.wkt;
    requires science.unlicense.time;
    requires science.unlicense.encoding;
    requires science.unlicense.system.jvm;
    requires science.unlicense.binding.json;
    requires science.unlicense.common;
    requires science.unlicense.media;
    requires science.unlicense.syntax;
    requires science.unlicense.system;
    requires science.unlicense.engine.ui;
    requires science.unlicense.concurrent;
    requires science.unlicense.image;
    requires science.unlicense.display;
    requires science.unlicense.math;
    requires science.unlicense.task;
    requires science.unlicense.geometry;
    requires science.unlicense.engine.swing;
    exports science.unlicense.p.editbit;
}